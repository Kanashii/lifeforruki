package com.vbax.life.model

/**
 * Created by Victoria Bakhaeva on 21-Mar-20.
 */
enum class LifeElement {
        AliveCell,
        DeadCell,
        Life,
        Death
}