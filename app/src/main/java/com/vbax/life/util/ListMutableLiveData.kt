package com.vbax.life.util

import androidx.lifecycle.MutableLiveData

/**
 * Created by Victoria Bakhaeva on 21-Mar-20.
 */
class ListMutableLiveData<T> : MutableLiveData<List<T>>() {
    fun addValue(element: T) {
        value = value?.plus(element) ?: listOf(element)
    }

}