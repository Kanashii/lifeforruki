package com.vbax.life.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vbax.life.R
import com.vbax.life.model.LifeElement
import kotlinx.android.synthetic.main.main_fragment.*


class CellsListFragment : Fragment() {

    companion object {
        fun newInstance() = CellsListFragment()
    }

    private lateinit var viewModel: MainViewModel
    private var adapter = LifeElementsAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        addCellBtn.setOnClickListener { viewModel.createCell() }
        lifeElementsList.adapter = adapter
        viewModel.lifeElements.observe(this, Observer {
            adapter.submitList(it)
        })
        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                (lifeElementsList.layoutManager as LinearLayoutManager).smoothScrollToPosition(
                    lifeElementsList,
                    null,
                    adapter.itemCount
                )
            }
        })
    }

    class LifeElementsAdapter : RecyclerView.Adapter<LifeElementsAdapter.LifeElementVH>() {
        private var list: List<LifeElement> = ArrayList()
        fun submitList(newList: List<LifeElement>) {
            val addedElements = newList.size - list.size
            list = newList
            if (addedElements > 0) {
                notifyItemRangeInserted(list.size - addedElements, addedElements)
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LifeElementVH {
            return LifeElementVH(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.cell_list_item,
                    parent,
                    false
                )
            )
        }

        override fun getItemCount(): Int = list.size

        override fun onBindViewHolder(holder: LifeElementVH, position: Int) {
            holder.bind(list[position])
        }

        class LifeElementVH(view: View) : RecyclerView.ViewHolder(view) {
            private val icon: ImageView = view.findViewById(R.id.elementIcon)
            private val title: TextView = view.findViewById(R.id.elementTitle)
            private val subTitle: TextView = view.findViewById(R.id.elementSubtitle)

            fun bind(lifeElement: LifeElement) {
                when (lifeElement) {
                    LifeElement.AliveCell -> bindAliveCell()
                    LifeElement.DeadCell -> bindDeadCell()
                    LifeElement.Death -> bindDeath()
                    LifeElement.Life -> bindLife()
                }
            }

            private fun bindAliveCell() {
                icon.setImageResource(R.drawable.ic_alive)
                icon.setBackgroundResource(R.drawable.icon_bg_alive_cell)
                title.setText(R.string.title_alive_cell)
                subTitle.setText(R.string.subtitle_alive_cell)
            }

            private fun bindDeadCell() {
                icon.setImageResource(R.drawable.ic_dead)
                icon.setBackgroundResource(R.drawable.icon_bg_dead_cell)
                title.setText(R.string.title_dead_cell)
                subTitle.setText(R.string.subtitle_dead_cell)
            }

            private fun bindLife() {
                icon.setImageResource(R.drawable.ic_life)
                icon.setBackgroundResource(R.drawable.icon_bg_life)
                title.setText(R.string.title_life)
                subTitle.setText(R.string.subtitle_life)
            }

            private fun bindDeath() {
                icon.setImageResource(R.drawable.ic_death)
                icon.setBackgroundResource(R.drawable.icon_bg_death)
                title.setText(R.string.title_death)
                subTitle.setText(R.string.subtitle_death)
            }
        }

    }

}





