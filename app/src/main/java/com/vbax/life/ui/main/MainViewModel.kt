package com.vbax.life.ui.main

import androidx.lifecycle.ViewModel
import com.vbax.life.model.LifeElement
import com.vbax.life.util.ListMutableLiveData
import kotlin.random.Random

class MainViewModel : ViewModel() {

    val lifeElements: ListMutableLiveData<LifeElement> by lazy {
        ListMutableLiveData<LifeElement>().also {
            it.value = ArrayList()
        }
    }

    fun createCell() {
        addLifeElement(randomCell())
        createLifeIfNecessary()
    }

    private fun addLifeElement(lifeElement: LifeElement) {
        lifeElements.addValue(lifeElement)
    }

    private fun createLifeIfNecessary() {
        val elementsReversed = lifeElements.value?.reversed()
        if (elementsReversed?.size ?: 0 < 3) {
            return
        }
        val lastElement = elementsReversed!![0]
        if (elementsReversed[1] == lastElement && elementsReversed[2] == lastElement) {
            addLifeElement(createLifeOrDeath(lastElement))
        }
    }

    private fun createLifeOrDeath(cell: LifeElement): LifeElement {
        if (cell == LifeElement.AliveCell)
            return LifeElement.Life
        if (cell == LifeElement.DeadCell)
            return LifeElement.Death
        throw IllegalStateException("Life and death should be created only with cells")
    }

    private fun randomCell(): LifeElement =
        if (Random.nextBoolean()) LifeElement.AliveCell else LifeElement.DeadCell

}
