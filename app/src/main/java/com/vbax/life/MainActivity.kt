package com.vbax.life

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.vbax.life.ui.main.CellsListFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, CellsListFragment.newInstance())
                    .commitNow()
        }
    }
}
